import { AxiosResponse } from 'axios';
import fileServer from 'file-saver';
import BaseApiService from './BaseApiService';

class UserService extends BaseApiService {
  public currentUser(): Promise<any> {
    return this.http.get<any>('/users/current')
      .then((x: AxiosResponse<string>) => x.data);
  }

  public uploadFiles(files: any) {
    files.forEach((file: any) => {
      const formData = new FormData();
      formData.append('file', file, file.name);
      this.postDocCreate(formData);
    });
  }

  public download() {
    return new Promise((resolve, reject) => {
      this.http.get('/audio?author=quake&title=jump', {
        responseType: 'blob',
        headers: { Range: 'bytes=0-12930012' },
      }).then((response) => {
        fileServer.saveAs(response.data, 'fileName.wav');
        resolve(true);
      }).catch(reject);
    });
    // return this.http.get<any>('/audio?author=quake&title=jump', {
    //   responseType: 'blob',
    // })
    //   .then((x: AxiosResponse<any>) => {
    //     fileServer.saveAs(x.data, 'fileName.wav');
    //   });
  }

  public getFile() {
    return this.http.get('/audio?author=quake&title=jump', {
      responseType: 'blob',
      headers: { Range: 'bytes=0-12930012' },
    })
      .then((x: AxiosResponse<any>) => {
        const blob = new Blob([x.data], { type: 'audio/mp3' });
        const url: string = window.URL.createObjectURL(blob);
        return url;
      });
  }

  public postDocCreate(formData: any) {
    return this.http.post<any>('/audio?author=quake&title=jump', formData)
      .then((x: AxiosResponse<string>) => x.data);
  }
}

export default new UserService();
