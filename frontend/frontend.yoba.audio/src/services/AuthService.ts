import { AxiosResponse } from 'axios';
import LoginEntity from '@/entity/LoginEntity';
import BaseApiService from './BaseApiService';

class AuthService extends BaseApiService {
  public login(loginForm: LoginEntity): Promise<string> {
    return this.http.post<LoginEntity, AxiosResponse<any>>('/public/users/login', loginForm)
      .then((x: AxiosResponse<string>) => x.data);
  }

  public registration(loginForm: LoginEntity): Promise<string> {
    return this.http.post<LoginEntity, AxiosResponse<any>>('/public/users/register', loginForm)
      .then((x: AxiosResponse<string>) => x.data);
  }
}

export default new AuthService();
