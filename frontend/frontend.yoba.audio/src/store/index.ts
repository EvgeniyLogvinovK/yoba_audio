import { createStore } from 'vuex';
import UserEntity from '@/entity/UserEntity';
import LoginEntity from '@/entity/LoginEntity';
import UserService from '@/services/UserService';
import AuthService from '@/services/AuthService';
import TokenHelper from '@/helpers/TokenHelper';

export default createStore({
  state: {
    token: '',
    user: {
      id: '',
    } as UserEntity,
  },
  mutations: {
    setToken(state, payload: string) {
      state.token = payload;
    },
    setCurrrentUser(state, payload: UserEntity) {
      state.user = payload;
    },
  },
  actions: {
    async fetchCurrentUser({ commit }) {
      const user = await UserService.currentUser();
      commit('setCurrrentUser', user);
    },
    async login({ commit }, loginForm: LoginEntity) {
      const data: string = await AuthService.login(loginForm);
      TokenHelper.setToken('accessToken', data);
      commit('setToken', data);
    },
    async registration({ commit }, registrationForm: LoginEntity) {
      const data: string = await AuthService.registration(registrationForm);
      TokenHelper.setToken('accessToken', data);
      commit('setToken', data);
    },
    async uploadFiles({ commit }, files: any) {
      await UserService.uploadFiles(files);
    },
    async download() {
      await UserService.download();
    },
    async getFile({ commit }) {
      const src = await UserService.getFile();
      return src;
    },
  },
  modules: {
  },
});
