package com.petproject.yoba_audio.logic;

import com.petproject.yoba_audio.entities.AudioEntity;
import com.petproject.yoba_audio.entities.PrivateUserData;
import com.petproject.yoba_audio.services.AudioService;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.UUID;
import com.petproject.yoba_audio.dto.AudioDto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import static kotlin.collections.CollectionsKt.listOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.OK;

class AudioBusinessLogicTest {

    private final AudioEntity audio = new AudioEntity();

    private final AudioService audioService = mock(AudioService.class);
    private final RangeResolver rangeResolver = mock(RangeResolver.class);
    private final UserLogic userLogic = mock(UserLogic.class);
    private final AudioBusinessLogic audioBusinessLogic = new AudioBusinessLogic(
            audioService,
            userLogic,
            rangeResolver,
            "./testUploadFolder"
    );

    @BeforeEach
    public void setup() throws Exception {
        audio.setId(UUID.randomUUID());
        audio.setAuthor("someAuthor");
        audio.setTitle("someTitle");
        audio.setFileName("fileName.wav");
        audio.setCheckSum(audio.getAuthor() + audio.getTitle());

        when(audioService.getAudioFile(any()))
                .thenAnswer((InvocationOnMock invocation) -> new UrlResource(Paths.get(audio.getFileName()).toUri()));

        when(audioService.uploadAudioFile(any(), any(), any()))
                .thenAnswer((InvocationOnMock invocation) -> invocation.getArgument(2));

        when(rangeResolver.resolve(anyString(), any()))
                .thenAnswer((InvocationOnMock invocation) -> listOf(new ResourceRegion(invocation.getArgument(1), 0, 1)));

        when(userLogic.getCurrentUser()).thenReturn(new PrivateUserData());
    }

    @Test
    void download() throws Exception {
        ResponseEntity<StreamingResponseBody> download = audioBusinessLogic.download(audio.getId().toString(), "0-1");
        assertNotNull(download);
        assertEquals(OK, download.getStatusCode());
    }

    @Test
    void upload() throws IOException {
        final String author = "author";
        final String title = "title";
        final MultipartFile file = new MockMultipartFile("file.wav", "file.wav", "content-type", new byte[]{0});

        final AudioDto audioDto = audioBusinessLogic.upload(file, author, title);
        assertNotNull(audioDto);
        assertEquals(author, audioDto.getAuthor());
        assertEquals(title, audioDto.getTitle());
    }
}