package com.petproject.yoba_audio.controllers;


import com.petproject.yoba_audio.dto.AudioDto;
import com.petproject.yoba_audio.dto.FileUploadDto;
import com.petproject.yoba_audio.logic.AudioBusinessLogic;
import com.petproject.yoba_audio.validation.AudioValidator;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@RestController
@RequestMapping("/audio")
public class AudioController {

    private final AudioBusinessLogic audioBusinessLogic;

    private final AudioValidator audioValidator;

    public AudioController(AudioBusinessLogic audioBusinessLogic, AudioValidator audioValidator) {
        this.audioBusinessLogic = audioBusinessLogic;
        this.audioValidator = audioValidator;
    }

    @InitBinder("fileUploadDto")
    protected void initBinder(WebDataBinder binder) {
            binder.addValidators(audioValidator);
    }

    @GetMapping
    public ResponseEntity<StreamingResponseBody> download(@RequestParam String id,
                                                          @RequestHeader("Range") String range) throws Exception {
        return audioBusinessLogic.download(id, range);
    }

    @DeleteMapping
    public void deleteAudio(@RequestParam String id) {
        audioBusinessLogic.deleteAudio(UUID.fromString(id));
    }

    @GetMapping("/all")
    public List<AudioDto> getCurrentUserAudios() throws FileNotFoundException {
        return audioBusinessLogic.getCurrentUserAudios();
    }

    @PostMapping
    public AudioDto upload(@Valid @ModelAttribute FileUploadDto dto) throws IOException {
        return audioBusinessLogic.upload(dto.getFile(), dto.getAuthor(), dto.getTitle());
    }
}
