package com.petproject.yoba_audio.repositories;

import com.petproject.yoba_audio.entities.AudioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AudioFilesCRUDRepository extends CrudRepository<AudioEntity, UUID> {

    Optional<AudioEntity> findByCheckSum(String checkSum);

    Optional<List<AudioEntity>> findByOwner_id(UUID owner);

    Optional<AudioEntity> findByAuthorAndTitle(String author, String title);

}
