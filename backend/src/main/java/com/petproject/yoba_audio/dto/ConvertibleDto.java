package com.petproject.yoba_audio.dto;

import com.petproject.yoba_audio.entities.ConvertibleEntity;

public interface ConvertibleDto<T extends ConvertibleEntity> {
    T convertToEntity();
}
