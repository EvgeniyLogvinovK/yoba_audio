package com.petproject.yoba_audio.services;

import com.petproject.yoba_audio.dto.AudioDto;
import com.petproject.yoba_audio.entities.AudioEntity;
import com.petproject.yoba_audio.repositories.AudioFilesCRUDRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
public class AudioService {

    private final AudioFilesCRUDRepository audioFilesCRUDRepository;

    public AudioService(AudioFilesCRUDRepository audioFilesCRUDRepository,  @Value("${file.upload-path}") String path) {
        doInit(path);
        this.audioFilesCRUDRepository = audioFilesCRUDRepository;
    }

    private void doInit(final String path) {
        try {
            Files.createDirectories(Paths.get(path).toAbsolutePath().normalize());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void deleteAudio(UUID id) {
        this.audioFilesCRUDRepository.deleteById(id);
    }

    public Optional<AudioEntity> getAudioByCheckSum(String checkSum) {
        return audioFilesCRUDRepository.findByCheckSum(checkSum);
    }

    public Optional<List<AudioEntity>> getUserAudios(UUID userId) {

        return this.audioFilesCRUDRepository.findByOwner_id(userId);
    }

    public AudioEntity uploadAudioFile(final MultipartFile file, final Path path, final AudioEntity entity) throws IOException {
        Files.copy(file.getInputStream(), path, REPLACE_EXISTING);
        return audioFilesCRUDRepository.save(entity);
    }

    public Resource getAudioFile(UUID uuid) throws Exception {

        Optional<AudioEntity> audioFile = audioFilesCRUDRepository.findById(uuid);

        if(audioFile.isEmpty())
            throw new FileNotFoundException(String.format("Audio with id( %s ) not found!", uuid ));

        Path filePath = Paths.get(audioFile.get().getFileName());
        return new UrlResource(filePath.toUri());
    }

}
