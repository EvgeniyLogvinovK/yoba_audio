package com.petproject.yoba_audio.logic;

import java.util.List;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.http.HttpRange;
import org.springframework.stereotype.Component;

import static org.springframework.http.HttpRange.*;

@Component
public class RangeResolver {

    public List<ResourceRegion> resolve(final String range, final Resource resource) {
        List<HttpRange> ranges = parseRanges(range);
        return toResourceRegions(ranges, resource);
    }
}
