package com.petproject.yoba_audio.validation;

import com.petproject.yoba_audio.dto.FileUploadDto;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import static com.petproject.yoba_audio.enums.Error.NO_FILE_PROVIDED_MSG;
import static com.petproject.yoba_audio.enums.Error.ONLY_AUDIO_FILE_SUPPORTS_MSG;

@Component
public class AudioValidator implements Validator {

    private static final String ALLOWED_CONTENT_TYPE_REGEX = "^audio/.+$";

    @Override
    public boolean supports(final @NotNull Class<?> clazz) {
        return FileUploadDto.class.equals(clazz);
    }

    @Override
    public void validate(final @NotNull Object target,
                         final @NotNull Errors errors) {
        final MultipartFile file = ((FileUploadDto) target).getFile();

        if (file == null || file.isEmpty()) {
            errors.reject(NO_FILE_PROVIDED_MSG.getMessage());
        }

        final String contentType = file != null ? file.getContentType() : null;
        if (file != null && !isAllowedContentType(contentType)) {
            errors.reject(ONLY_AUDIO_FILE_SUPPORTS_MSG.getMessage());
        }
    }

    private boolean isAllowedContentType(String contentType) {
        return contentType != null && contentType.matches(ALLOWED_CONTENT_TYPE_REGEX);
    }
}
