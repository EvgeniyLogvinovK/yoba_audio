package com.petproject.yoba_audio

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class YobaAudioApplication

fun main(args: Array<String>) {
	runApplication<YobaAudioApplication>(*args)
}
