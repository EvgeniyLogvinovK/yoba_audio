package com.petproject.yoba_audio.enums

enum class Error(val message: String) {
    NO_FILE_PROVIDED_MSG("No file provided!"),
    ONLY_AUDIO_FILE_SUPPORTS_MSG("Only audio files are supported!")
}