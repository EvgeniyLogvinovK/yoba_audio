package com.petproject.yoba_audio.security.config

import com.petproject.yoba_audio.services.UserAuthenticationService
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component

@Component
class TokenAuthenticationProvider(
        private val auth: UserAuthenticationService
): AbstractUserDetailsAuthenticationProvider() {

    override fun additionalAuthenticationChecks(userDetails: UserDetails?, authentication: UsernamePasswordAuthenticationToken?) {
        //Nothing to do
    }

    override fun retrieveUser(username: String, authentication: UsernamePasswordAuthenticationToken): UserDetails =
            auth.findByToken(authentication.credentials.toString()).get()
}