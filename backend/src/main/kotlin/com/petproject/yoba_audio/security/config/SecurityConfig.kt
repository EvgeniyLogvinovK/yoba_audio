package com.petproject.yoba_audio.security.config

import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy.STATELESS
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter
import org.springframework.security.web.authentication.HttpStatusEntryPoint
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.security.web.util.matcher.NegatedRequestMatcher
import org.springframework.security.web.util.matcher.OrRequestMatcher
import org.springframework.security.web.util.matcher.RequestMatcher

private val PUBLIC_URLS: RequestMatcher = OrRequestMatcher(AntPathRequestMatcher("/public/**"))
private val PROTECTED_URLS: RequestMatcher = NegatedRequestMatcher(PUBLIC_URLS)

@Configuration
@EnableWebSecurity
class SecurityConfig(private val provider: TokenAuthenticationProvider): WebSecurityConfigurerAdapter() {

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(provider)
    }

    override fun configure(web: WebSecurity) {
        web.ignoring().requestMatchers(PUBLIC_URLS)
    }

    override fun configure(http: HttpSecurity) {
        http.cors()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(STATELESS)
                .and()
                .exceptionHandling() // this entry point handles when you request a protected page and you are not yet
                // authenticated
                .defaultAuthenticationEntryPointFor(forbiddenEntryPoint(), PROTECTED_URLS)
                .and()
                .authenticationProvider(provider)
                .addFilterBefore(restAuthenticationFilter(), AnonymousAuthenticationFilter::class.java)
                .authorizeRequests()
                .requestMatchers(PROTECTED_URLS)
                .authenticated()
                .and()
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .logout().disable()
    }


    @Bean
    fun restAuthenticationFilter(): TokenAuthenticationFilter = TokenAuthenticationFilter(PROTECTED_URLS).apply {
        setAuthenticationManager(authenticationManager())
        setAuthenticationSuccessHandler(successHandler())
    }

    @Bean
    fun successHandler(): SimpleUrlAuthenticationSuccessHandler =
            SimpleUrlAuthenticationSuccessHandler().apply { setRedirectStrategy(NoRedirectStrategy()) }

    /**
     * Disable Spring boot automatic filter registration.
     */
    @Bean
    fun disableAutoRegistration(filter: TokenAuthenticationFilter): FilterRegistrationBean<*> =
            FilterRegistrationBean(filter).apply { isEnabled = false }

    @Bean
    fun forbiddenEntryPoint(): AuthenticationEntryPoint = HttpStatusEntryPoint(FORBIDDEN)
}