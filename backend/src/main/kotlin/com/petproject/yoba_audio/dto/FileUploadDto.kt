package com.petproject.yoba_audio.dto

import org.springframework.web.multipart.MultipartFile

data class FileUploadDto(
    var author: String = "",
    val title: String = "",
    val file: MultipartFile? = null
)
