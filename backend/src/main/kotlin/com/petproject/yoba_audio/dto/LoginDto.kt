package com.petproject.yoba_audio.dto

data class LoginDto(val username: String, val password: String)