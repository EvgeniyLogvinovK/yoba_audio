package com.petproject.yoba_audio.token.jwt

import com.petproject.yoba_audio.token.TokenService
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Clock
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm.HS256
import io.jsonwebtoken.impl.TextCodec.BASE64
import io.jsonwebtoken.impl.compression.GzipCompressionCodec
import org.apache.commons.lang3.StringUtils.substringBeforeLast
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.Date
import java.util.function.Supplier

private const val DOT = "."
private val COMPRESSION_CODEC: GzipCompressionCodec = GzipCompressionCodec()

@Service
class JWTTokenService(
        @Value("\${jwt.issuer:yoba_audio}") private val issuer: String,
        @Value("\${jwt.expiration-sec:86400}") private val expirationSec: Long,
        @Value("\${jwt.clock-skew-sec:300}") private val clockSkewSec: Long,
        @Value("\${jwt.secret:secret}") secret: String
): Clock, TokenService {

    private val secretKey: String = BASE64.encode(secret)

    override fun permanent(attributes: Map<String, String>): String {
        return newToken(attributes, 0)
    }

    override fun expiring(attributes: Map<String, String>): String {
        return newToken(attributes, expirationSec)
    }

    private fun newToken(attributes: Map<String, String>, expiresInSec: Long): String {
        return Jwts.builder()
                .signWith(HS256, secretKey)
                .compressWith(COMPRESSION_CODEC)
                .setClaims(getClaims(attributes, expiresInSec))
                .compact()
    }

    private fun getClaims(attributes: Map<String, String>, expiresInSec: Long) =
            Jwts.claims().apply {
                this.issuer = this@JWTTokenService.issuer
                issuedAt = Date()
                putAll(attributes)
                if (expiresInSec > 0)
                    expiration = Date(System.currentTimeMillis() + expiresInSec * 1000)
            }

    override fun verify(token: String): Map<String, String> = parseClaims {
        Jwts.parser()
                .requireIssuer(issuer)
                .setClock(this)
                .setAllowedClockSkewSeconds(clockSkewSec)
                .setSigningKey(secretKey).parseClaimsJws(token).body
    }

    override fun untrusted(token: String): Map<String, String> =
            parseClaims { Jwts.parser()
                    .requireIssuer(issuer)
                    .setClock(this)
                    .setAllowedClockSkewSeconds(clockSkewSec)
                    .parseClaimsJwt(substringBeforeLast(token, DOT) + DOT).body
            }

    private fun parseClaims(toClaims: Supplier<Claims>): Map<String, String> = try {
        toClaims.get().map { (key, value) -> key to value.toString() }.toMap()
    } catch (e: IllegalArgumentException) {
        emptyMap()
    } catch (e: JwtException) {
        emptyMap()
    }

    override fun now(): Date = Date()
}