package com.petproject.yoba_audio.controllers

import com.petproject.yoba_audio.dto.LoginDto
import com.petproject.yoba_audio.logic.UserLogic
import com.petproject.yoba_audio.services.UserAuthenticationService
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.*

const val PUBLIC_CONTROLLER_PATH = "/public/users"
const val REGISTER = "/register"
const val LOGIN = "/login"

@RestController
@RequestMapping(PUBLIC_CONTROLLER_PATH)
class PublicUsersController(
        private val authentication: UserAuthenticationService,
        private val userLogic: UserLogic
) {

    @GetMapping("/hello")
    fun helloEndpoint() = "Hello Endpoint"

    @PostMapping(value = [REGISTER], consumes = [APPLICATION_JSON_VALUE])
    fun register(@RequestBody loginDto: LoginDto): String {
        userLogic.register(loginDto.username, loginDto.password)
        return login(loginDto)
    }

    @PostMapping(LOGIN)
    fun login(@RequestBody loginDto: LoginDto): String =
            authentication.login(loginDto.username, loginDto.password)
                    .orElseThrow { RuntimeException("invalid login and/or password") }
}