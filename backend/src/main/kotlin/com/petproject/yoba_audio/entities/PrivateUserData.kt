package com.petproject.yoba_audio.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.GenericGenerator
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import javax.persistence.*

@Entity
data class PrivateUserData(
        @OneToOne
        @JoinColumn(name = "publicUserData_id")
        private var publicUserData: PublicUserData? = null,

        @Column(name = "userName")
        private var username: String = "",

        @Column(name = "password")
        private var password: String = ""
): UserDetails {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    var id: UUID? = null

    @JsonIgnore
    override fun getAuthorities(): MutableCollection<out GrantedAuthority>? = null

    @JsonIgnore
    override fun getPassword(): String = password

    @JsonIgnore
    override fun getUsername(): String = username

    @JsonIgnore
    override fun isAccountNonExpired(): Boolean = true

    @JsonIgnore
    override fun isAccountNonLocked(): Boolean = true

    @JsonIgnore
    override fun isCredentialsNonExpired(): Boolean = true

    @JsonIgnore
    override fun isEnabled(): Boolean = true
}