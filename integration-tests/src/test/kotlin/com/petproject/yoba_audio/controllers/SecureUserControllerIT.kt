package com.petproject.yoba_audio.controllers

import com.petproject.yoba_audio.config.SpringConfigForITs
import com.petproject.yoba_audio.config.TEST_PROFILE
import com.petproject.yoba_audio.config.WithContainer
import com.petproject.yoba_audio.utils.TestUtils
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

private const val USER_NAME = "uname"
private const val PASSWORD = "pwd"

@AutoConfigureMockMvc
@ActiveProfiles(TEST_PROFILE)
@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [SpringConfigForITs::class])
class SecureUserControllerIT: WithContainer {

    @Autowired
    lateinit var testUtils: TestUtils

    @Test
    fun `Get Current User`() {
        val token = testUtils.register(USER_NAME, PASSWORD).andReturn().response.contentAsString
        testUtils.getCurrentUser(token).andExpect(status().isOk).andDo(print())
    }
}