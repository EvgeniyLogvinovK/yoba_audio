package com.petproject.yoba_audio.controllers

import com.petproject.yoba_audio.config.SpringConfigForITs
import com.petproject.yoba_audio.config.TEST_PROFILE
import com.petproject.yoba_audio.config.WithContainer
import com.petproject.yoba_audio.repositories.UserRepository
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import com.petproject.yoba_audio.utils.TestUtils

private const val USER_NAME = "uname"
private const val PASSWORD = "pwd"

@AutoConfigureMockMvc
@ActiveProfiles(TEST_PROFILE)
@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [SpringConfigForITs::class])
class PublicUserControllerIT: WithContainer {

    @Autowired
    lateinit var testUtils: TestUtils

    @Autowired
    private lateinit var userRepository: UserRepository

    @AfterEach
    fun tearDown() {
        userRepository.deleteAll()
    }

    @Test
    fun `Registration Success`() {
        testUtils.register(USER_NAME, PASSWORD).andExpect(status().isOk)
    }

    @Test
    fun `Registration Fail`() {
        testUtils.register(USER_NAME, PASSWORD).andExpect(status().isOk)
        testUtils.register(USER_NAME, PASSWORD).andExpect(status().isBadRequest)
    }

    @Test
    fun `Login Success`() {
        testUtils.register(USER_NAME, PASSWORD)
        testUtils.login(USER_NAME, PASSWORD).andExpect(status().isOk)
    }

    @Test
    fun `Login Fail`() {
        testUtils.login(USER_NAME, PASSWORD).andExpect(status().isInternalServerError)
    }
}