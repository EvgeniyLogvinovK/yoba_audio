package com.petproject.yoba_audio.config

import org.junit.ClassRule
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer

interface WithContainer {

    companion object {
        @ClassRule
        val container = PostgreSQLContainer<Nothing>("postgres").apply {
            withDatabaseName("testdb")
            withUsername("duke")
            withPassword("s3crEt")
            start()
        }

        @JvmStatic
        @DynamicPropertySource
        fun properties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", container::getJdbcUrl);
            registry.add("spring.datasource.password", container::getPassword);
            registry.add("spring.datasource.username", container::getUsername);
        }
    }
}