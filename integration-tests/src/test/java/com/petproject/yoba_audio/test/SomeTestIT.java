package com.petproject.yoba_audio.test;

import com.petproject.yoba_audio.config.SpringConfigForITs;
import com.petproject.yoba_audio.config.WithContainer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.petproject.yoba_audio.config.SpringConfigForITsKt.TEST_PROFILE;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@AutoConfigureMockMvc
@ActiveProfiles(TEST_PROFILE)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = SpringConfigForITs.class)
public class SomeTestIT implements WithContainer {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void javaTest() {
        String answer = restTemplate
                .withBasicAuth("spring", "secret")
                .getForObject("http://localhost:" + port + "/public/users/hello", String.class);
        System.out.println("Answer: " + answer);
    }
}
